1. Цикл в програмуванні — це конструкція, яка дозволяє виконувати певний блок коду n разів, поки задана умова є істинною.
   Цикли дають змогу автоматизувати завдання, які вимагають багаторазового повторення одних і тих самих дій.

2. Цикл "for"
   Використовується для виконання певного блоку коду заздалегідь визначену кількість разів.

   Цикл "while"
   Використовується для виконання певного блоку коду, поки задана умова виконується.

   Цикл "do while"
   Подібний до циклу "while", але виконується блок коду спочатку, а потім перевіряється умова. Тому, цей цикл завжди виконається принаймні один раз.

3. Єдиною відмінністю циклу "do while" від циклу "while" є саме те, що тіло циклу в циклі "do while" виконується принаймні один раз.
   Цикл "do while" зазвичай використовують саме тоді, коли немає потреби перевіряти умову, доки тіло циклу не виконано.
